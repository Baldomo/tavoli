package json;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import data.Ordine;

import java.lang.reflect.Type;

/**
 * Created by Leonardo Baldin on 13/09/17.
 *
 * @author Leonardo Baldin
 */
public class OrdineDeserializer implements JsonDeserializer<Ordine> {
    @Override
    public Ordine deserialize(JsonElement src, Type type, JsonDeserializationContext context) throws JsonParseException {
        return new Ordine(src.getAsJsonObject().get("item").getAsString(), src.getAsJsonObject().get("prezzo").getAsDouble());
    }
}
