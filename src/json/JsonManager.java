package json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import data.Ordine;
import data.Tavolo;
import ui.UIStarter;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Leonardo Baldin on 11/09/17.
 *
 * @author Leonardo Baldin
 */
@SuppressWarnings("unchecked")
public class JsonManager {

    private static String jsonFilePath = "src/assets/tavoli.json";
    private static Gson gson;

    private static ArrayList<Tavolo> jsonTavoli = new ArrayList<>();
    private static Type arrayType = new TypeToken<ArrayList<Tavolo>>(){}.getType();
    private static Type ordineType = new TypeToken<Ordine>(){}.getType();

    static {
        gson = new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithoutExposeAnnotation()
                .registerTypeAdapter(ordineType, new OrdineSerializer())
                .registerTypeAdapter(ordineType, new OrdineDeserializer())
                .create();

        refresh();
    }

    private static File fetchFile() {
        try {
            return new File(UIStarter.class.getClassLoader().getResource(jsonFilePath).toURI());
        } catch (URISyntaxException | NullPointerException e) {
            e.printStackTrace();
        }
        System.out.println("Errore lettura json! fetchFile()");
        return null;
    }

    private static void refresh() {
        try (JsonReader jsonReader = new JsonReader(new FileReader(jsonFilePath))) {
            jsonTavoli = gson.fromJson(jsonReader, arrayType);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Errore lettura json!");
        }
    }

    public static void addAll(Tavolo... tavoli) {
        ArrayList<Tavolo> buf = new ArrayList<>(Arrays.asList(tavoli));
        buf.forEach(tavolo -> {
            if (recordExists(tavolo)) buf.remove(tavolo);
            else {
                //lastId += 1;
                tavolo.setId(getNextId());
            }
        });
        jsonTavoli.addAll(buf);
        try (JsonWriter jsonWriter = new JsonWriter(new FileWriter(jsonFilePath))) {
            jsonWriter.setIndent("  ");
            gson.toJson(jsonTavoli, arrayType, jsonWriter);
            jsonWriter.close();
            refresh();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Errore scrittura json!");
        }
    }

    public static void change(int id, Tavolo newTavolo) {
        //if (!recordExists(jsonTavoli.get(id))) throw new IllegalArgumentException("Il tavolo a id = " + id + " non esiste");
        jsonTavoli.set(id, newTavolo);
        flush();
    }

    public static void removeAll(Tavolo... tavoli) {
        jsonTavoli.removeAll(Arrays.asList(tavoli));
        try (JsonWriter jsonWriter = new JsonWriter(new FileWriter(jsonFilePath))) {
            jsonWriter.setIndent("  ");
            gson.toJson(jsonTavoli, arrayType, jsonWriter);
            jsonWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Errore scrittura json!");
        }
    }

    private static void flush() {
        try (JsonWriter jsonWriter = new JsonWriter(new FileWriter(jsonFilePath))) {
            jsonWriter.setIndent("  ");
            gson.toJson(jsonTavoli, arrayType, jsonWriter);
            jsonWriter.close();
            refresh();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Errore scrittura json!");
        }
    }

    public static Tavolo get(int id) {
        refresh();
        return jsonTavoli.get(id);
    }

    public static Tavolo last() {
        refresh();
        return jsonTavoli.get(jsonTavoli.size() - 1);
    }

    public static ArrayList<Tavolo> getAll() {
        refresh();
        return jsonTavoli;
    }

    public static int getNextId() {
        refresh();
        if (jsonTavoli.isEmpty()) return 0;
        return last().getId() + 1;
    }

    public static boolean recordExists(Tavolo tavolo) {
        refresh();
        return jsonTavoli.contains(tavolo);
    }

    public static String peek() {
        refresh();
        return (jsonTavoli.get(jsonTavoli.size() - 1) == null) ? jsonTavoli.get(jsonTavoli.size() - 1).toString() : "Json vuoto!";
    }

}
