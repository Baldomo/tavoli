package json;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import data.Ordine;

import java.lang.reflect.Type;

/**
 * Created by Leonardo Baldin on 13/09/17.
 *
 * @author Leonardo Baldin
 */
public class OrdineSerializer implements JsonSerializer<Ordine>{
    @Override
    public JsonElement serialize(Ordine src, Type type, JsonSerializationContext context) {
        JsonObject jsonOrdine = new JsonObject();
        jsonOrdine.addProperty("item", src.getItemProperty().getValue());
        jsonOrdine.addProperty("prezzo", src.getPrezzoProperty().getValue());

        return jsonOrdine;
    }
}
