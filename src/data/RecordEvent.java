package data;

import javafx.event.Event;
import javafx.event.EventTarget;
import javafx.event.EventType;

/**
 * Created by Leonardo Baldin on 25/08/17.
 *
 * @author Leonardo Baldin
 */
public class RecordEvent extends Event {

    public static final EventType<RecordEvent> ROOT_EVENT = new EventType<>(Event.ANY, "ROOT_EVENT");
    public static final EventType<RecordEvent> RECORD_DELETED = new EventType<>(ROOT_EVENT, "RECORD_DELETED");
    public static final EventType<RecordEvent> RECORD_ADDED = new EventType<>(ROOT_EVENT, "RECORD_ADDED");
    public static final EventType<RecordEvent> RECORD_MODIFIED = new EventType<>(ROOT_EVENT, "RECORD_MODIFIED");

    private Object source;

    public RecordEvent(EventType<? extends Event> eventType) {
        super(eventType);
    }

    public RecordEvent(Object source, EventTarget target, EventType<? extends Event> eventType) {
        super(source, target, eventType);
        this.source = source;
    }

    @Override
    public Object getSource() {
        return source;
    }
}
