package data;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by Leonardo Baldin on 09/09/17.
 *
 * @author Leonardo Baldin
 */
public final class Ordine extends RecursiveTreeObject<Ordine> {
    private final StringProperty itemProperty;
    private final SimpleDoubleProperty prezzoProperty;

    public Ordine(String item, double prezzo) {
        this.itemProperty = new SimpleStringProperty(item);
        this.prezzoProperty = new SimpleDoubleProperty(prezzo);
    }

    public StringProperty getItemProperty() {
        return itemProperty;
    }

    public SimpleDoubleProperty getPrezzoProperty() {
        return prezzoProperty;
    }

    @Override
    public String toString() {
        return "Ordine {" +
                "item='" + itemProperty.getValue() + '\'' +
                ", prezzo=" + prezzoProperty.getValue() +
                '}';
    }
}
