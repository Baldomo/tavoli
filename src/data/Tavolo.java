package data;

import com.google.gson.annotations.Expose;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.paint.Color;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Random;

/**
 * Created by Leonardo Baldin on 11/08/17.
 *
 * @author Leonardo Baldin
 */
public class Tavolo {

    @Expose private int id;
    @Expose private int persone;
    @Expose private int posti;
    @Expose private float conto;

    @Expose private double a, r, g, b;

    @Expose private ArrayList<Ordine> ordine = new ArrayList<>();

    public Tavolo() {
        this.persone = 0;
        this.posti = 4;
        this.conto = 0.0f;

        this.a = 1.0;
        this.r = 1.0;
        this.g = 1.0;
        this.b = 1.0;

        randomOrdini(this, 10);
    }

    public int getId() {
        return id;
    }

    public Tavolo setId(int id) {
        this.id = id;
        return this;
    }

    public int getPersone() {
        return persone;
    }

    public Tavolo setPersone(int persone) {
        if (persone < 0) throw new IndexOutOfBoundsException("Il numero di persone dev'essere maggiore o uguale a 0");
        if (persone > posti) posti = persone;
        this.persone = persone;
        return this;
    }

    public int getPosti() {
        return posti;
    }

    public Tavolo setPosti(int posti) {
        if (posti <= 0) throw new IndexOutOfBoundsException("Il numero di posti dev'essere maggiore di 0");
        if (posti < persone) posti = persone;
        this.posti = posti;
        return this;
    }

    public float getConto() {
        return conto;
    }

    public Tavolo setConto(float conto) {
        if (conto < 0) throw new IndexOutOfBoundsException("Il conto dev'essere maggiore o uguale a 0");
        this.conto = conto;
        return this;
    }

    public final ObservableList<Ordine> getOrdini() {
        return FXCollections.observableArrayList(ordine);
    }

    public Tavolo setOrdini(Collection<? extends Ordine> ordini) {
        this.ordine = new ArrayList<>(ordine);
        return this;
    }

    public Tavolo addOrdini(Ordine... ordini) {
        this.ordine.addAll(Arrays.asList(ordini));
        return this;
    }

    public Tavolo addOrdini(Collection<? extends Ordine> c) {
        this.ordine.addAll(c);
        return this;
    }

    public Tavolo removeOrdini(Ordine... ordini) {
        this.ordine.removeAll(Arrays.asList(ordini));
        return this;
    }

    public Tavolo removeOrdini(Collection<? extends Ordine> c) {
        this.ordine.removeAll(c);
        return this;
    }

    public Color getColor() {
        return Color.color(r, g, b, a);
    }

    public Tavolo setColor(Color color) {
        this.a = color.getOpacity();
        this.r = color.getRed();
        this.g = color.getGreen();
        this.b = color.getBlue();
        return this;
    }

    //////////////
    //  Utility //
    //////////////

    public boolean isFull() {
        return posti == persone;
    }

    public boolean isEmpty() {
        return persone == 0;
    }

    @Override
    public String toString() {
        return id + ": [ " + posti + " posti, " + persone + " persone ]";
    }

    public static void randomOrdini(Tavolo tavolo, int many) {
        final Random rand = new SecureRandom();
        final String[] names = {"Pasta al ragù", "Purè", "Carne fritta",
                "Filetto di agnello", "Dito umano", "Aria", "Non c'è più vino", "Coca Cola", "Polpette", "Acqua", "Crauti",
                "Pesce volante", "Calamari", "Gamberi in padella", "Lo chef"};

        for (int i = 0; i <= many; i++) {
            tavolo.addOrdini(new Ordine(names[rand.nextInt(names.length)], (Math.round(rand.nextDouble() * 100.0) / 100.0) * 10));
        }
    }
}
