package data;

import com.jfoenix.controls.JFXButton;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.text.Text;
import json.JsonManager;
import ui.MaterialCard;

import java.util.ArrayList;

/**
 * Created by Leonardo Baldin on 20/08/17.
 *
 * @author Leonardo Baldin
 */
public final class CardManager {

    private static final double CARD_SIZE = 150.0;

    public static ObservableList<MaterialCard> getAllCards() {
        ArrayList<Tavolo> buf = new ArrayList<>(JsonManager.getAll());
        ObservableList<MaterialCard> list = FXCollections.observableArrayList();

        if (buf.isEmpty()) buf = new ArrayList<>(JsonManager.getAll());
        buf.forEach(tavolo -> list.add(toCard(tavolo)));

        return list;
    }

    public static MaterialCard getLastCard() {
        return toCard(JsonManager.last());
    }

    private static MaterialCard toCard(Tavolo tavolo) {
        return new MaterialCard(CARD_SIZE, "Tavolo " + (tavolo.getId() + 1), tavolo)
                .setHeaderText(new Text("Persone: " + tavolo.getPersone()))
                .setFooterText(new Text("Posti: " + tavolo.getPosti()))
                .setBackground(tavolo.getColor())
                .setActions(new JFXButton("Modifica"), new JFXButton("Elimina"));
    }
}
