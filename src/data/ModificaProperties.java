package data;

import javafx.scene.paint.Color;

import java.util.List;

/**
 * Created by Leonardo Baldin on 09/09/17.
 *
 * @author Leonardo Baldin
 */
public class ModificaProperties {
    private String persone;
    private String posti;
    private Color color;
    private List<Ordine> ordini;

    private ModificaProperties() {}

    public ModificaProperties(String persone, String posti, Color color, List<Ordine> ordini) {
        this.persone = persone;
        this.posti = posti;
        this.color = color;
        this.ordini = ordini;
    }

    public String getPersone() {
        return persone;
    }

    public ModificaProperties setPersone(String persone) {
        this.persone = persone;
        return this;
    }

    public String getPosti() {
        return posti;
    }

    public ModificaProperties setPosti(String posti) {
        this.posti = posti;
        return this;
    }

    public Color getColor() {
        return color;
    }

    public ModificaProperties setColor(Color color) {
        this.color = color;
        return this;
    }

    public List<Ordine> getOrdini() {
        return ordini;
    }

    public ModificaProperties setOrdini(List<Ordine> ordini) {
        this.ordini = ordini;
        return this;
    }
}
