package ui;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXMasonryPane;
import com.jfoenix.controls.JFXTextField;
import data.CardManager;
import data.RecordEvent;
import data.Tavolo;
import javafx.animation.FadeTransition;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;
import json.JsonManager;

import java.net.URL;
import java.util.ResourceBundle;

public class UIController implements Initializable {

    @FXML private SplitPane mainSplitPane;
    @FXML private GridPane leftPane;
    @FXML private ScrollPane rightScrollPane;
    @FXML private JFXMasonryPane rightContent;

    private JFXTextField persone = new JFXTextField();
    private JFXTextField posti = new JFXTextField();
    private Button conferma = new JFXButton("Aggiungi");

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        double dividerPos = mainSplitPane.getDividerPositions()[0];
        leftPane.maxWidthProperty().bind(mainSplitPane.widthProperty().multiply(dividerPos));
        leftPane.minWidthProperty().bind(mainSplitPane.widthProperty().multiply(dividerPos));

        rightScrollPane.setFitToHeight(true);
        rightScrollPane.setFitToWidth(true);
        rightContent.setPadding(new Insets(8.0));
        rightContent.setLayoutMode(JFXMasonryPane.LayoutMode.BIN_PACKING);
        rightContent.addEventHandler(RecordEvent.RECORD_DELETED, event -> fadeOut((MaterialCard) event.getSource()));

        leftPane.setPadding(new Insets(8.0, 8.0, 8.0, 8.0));
        leftPane.setAlignment(Pos.TOP_CENTER);
        leftPane.setHgap(8);
        leftPane.setVgap(10);

        persone.setPromptText("Persone");
        persone.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.equals("")) {
                if (!newValue.matches("\\d*")) persone.setText(newValue.replaceAll("[^\\d]", ""));
                if (Integer.parseInt(newValue) < 0) persone.setText("1");
                if (Integer.parseInt(newValue) > 30) persone.setText("30");
            }
        });
        posti.setPromptText("Posti");
        posti.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.equals("")) {
                if (!newValue.matches("\\d*")) posti.setText(newValue.replaceAll("[^\\d]", ""));
                if (Integer.parseInt(newValue) < 0) posti.setText("1");
                if (Integer.parseInt(newValue) > 30) posti.setText("30");
            }
        });
        conferma.setStyle(
                "-fx-padding: 0.7em 0.57em;" +
                "-fx-font-size: 14px;" +
                "-jfx-button-type: RAISED;" +
                "-fx-text-fill: WHITE;"
        );
        conferma.setBackground(new Background(new BackgroundFill(Color.DARKVIOLET, CornerRadii.EMPTY, Insets.EMPTY)));
        conferma.setOnMouseClicked(event -> {
            if (!persone.getText().isEmpty() && !posti.getText().isEmpty()) {
                JsonManager.addAll(
                        new Tavolo()
                                .setPersone(Integer.parseInt(persone.getText()))
                                .setPosti(Integer.parseInt(posti.getText()))
                );
                fadeIn(CardManager.getLastCard());
                conferma.fireEvent(new RecordEvent(conferma, null, RecordEvent.RECORD_ADDED));
            }
        });

        Rectangle toolbar = new Rectangle();
        toolbar.widthProperty().bind(leftPane.widthProperty());
        toolbar.setHeight(50);
        toolbar.setFill(Color.BLUEVIOLET);
        toolbar.setStyle("-fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.4), 3, 0, 0, 3);");
        GridPane.setColumnSpan(toolbar, GridPane.REMAINING);
        leftPane.add(toolbar, 0, 0);
        leftPane.add(new Label("Persone:"), 0, 1);
        leftPane.add(persone, 1, 1);
        leftPane.add(new Label("Posti:"), 0, 2);
        leftPane.add(posti, 1, 2);
        leftPane.add(conferma, 0, 3);
        GridPane.setColumnSpan(conferma, GridPane.REMAINING);
        GridPane.setHgrow(conferma, Priority.ALWAYS);

        if (JsonManager.getAll().isEmpty()) rightScrollPane.setContent(emptyPane());
        CardManager.getAllCards().forEach(this::fadeIn);
    }

    private VBox emptyPane() {
        VBox emptyBox = new VBox();
        emptyBox.setAlignment(Pos.TOP_CENTER);
        emptyBox.setSpacing(10);
        emptyBox.setPadding(new Insets(20,0,0,0));

        ImageView boxView = new ImageView();
        boxView.setFitHeight(80);
        boxView.setFitWidth(80);
        boxView.setImage(new Image("/assets/empty-box.png"));
        emptyBox.getChildren().add(boxView);

        Label nothing = new Label("Sembra che qui non ci sia nulla.");
        nothing.setStyle("-fx-text-fill: #b1b1b1; -fx-font-style: italic");
        emptyBox.getChildren().add(nothing);

        return emptyBox;
    }

    public void fadeOut(MaterialCard card) {
        FadeTransition fadeOut = new FadeTransition(Duration.millis(300));
        fadeOut.setNode(card);
        fadeOut.setFromValue(1.0);
        fadeOut.setToValue(0.0);
        fadeOut.setCycleCount(1);
        fadeOut.setAutoReverse(false);
        fadeOut.playFromStart();

        fadeOut.setOnFinished(end -> {
            rightContent.getChildren().remove(card);
            if (rightContent.getChildren().isEmpty()) rightScrollPane.setContent(emptyPane());
        });
    }

    public void fadeIn(MaterialCard card) {
        FadeTransition fadeIn = new FadeTransition(Duration.millis(400));
        fadeIn.setNode(card);
        fadeIn.setFromValue(1.0);
        fadeIn.setToValue(0.0);
        fadeIn.setCycleCount(1);
        fadeIn.setAutoReverse(false);
        fadeIn.playFromStart();

        fadeIn.setOnFinished(end -> {
            if (rightContent.getChildren().isEmpty()) {
                rightScrollPane.setContent(null);
                rightScrollPane.setContent(rightContent);
            }
            rightContent.getChildren().add(card);
        });
    }
}