package ui;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class UIStarter extends Application {

    public static void main(String[] args) {
        Application.launch(UIStarter.class, args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("ui/root.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root, 300, 275);
        primaryStage.setTitle("Tavoli");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    @Override
    public final void stop() {
        Platform.exit();
    }

}
