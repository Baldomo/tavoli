package ui;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRippler;
import com.jfoenix.effects.JFXDepthManager;
import data.ModificaProperties;
import data.RecordEvent;
import data.Tavolo;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.control.ButtonType;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import json.JsonManager;

import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Leonardo Baldin on 04/03/2017.
 *
 * @author Leonardo Baldin
 */
public class MaterialCard extends Pane {

    private double height;
    private double width;

    private GridPane layout = new GridPane();
    private JFXRippler rippler;

    private static final String STANDARD_STYLE =
            "-fx-border-radius: 2px; "; // +
            //"-fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.4), 3, 0, 0, 3);";

    private Text title = new Text();
    private Text header = new Text();
    private Text footer = new Text();

    private Tavolo bound;

    private MaterialCard() {
        super();
        this.setStyle(STANDARD_STYLE);
        JFXDepthManager.setDepth(this, 1);

        rippler = new JFXRippler();
        rippler.setRipplerFill(Color.LIGHTGRAY);
        this.getChildren().add(rippler);

        layout.setPadding(new Insets(5, 5, 5, 5));
        layout.setVgap(5);
        layout.setHgap(5);
        initializeListeners();
    }

    public MaterialCard(double height, double width, String title, Tavolo bind) {
        this();
        this.height = height;
        this.width = width;
        this.setPrefHeight(height);
        this.setPrefWidth(width);

        this.setTitle(new Text(title));
        this.bound = bind;
        if (this.bound.isFull()) this.setTitle(new Text(this.title.getText() + " - PIENO"));

        layout.prefHeightProperty().bind(this.heightProperty());
        layout.prefWidthProperty().bind(this.widthProperty());
    }

    public MaterialCard(double side, String title, Tavolo bind) {
        this();
        this.height = side;
        this.width = side;
        this.setPrefHeight(side);
        this.setPrefWidth(side);

        this.setTitle(new Text(title));
        this.bound = bind;
        if (this.bound.isFull()) this.setTitle(new Text(this.title.getText() + " - PIENO"));

        layout.prefHeightProperty().bind(this.heightProperty());
        layout.prefWidthProperty().bind(this.widthProperty());
    }

    public final MaterialCard disableRippler() {
        this.getChildren().remove(rippler);
        return this;
    }

    public final MaterialCard enableRippler() {
        if (this.getChildren().contains(rippler)) this.getChildren().remove(rippler);
        this.getChildren().add(rippler);
        return this;
    }

    public final MaterialCard setRipplerFill(Paint color) {
        rippler.setRipplerFill(color);
        return this;
    }

    public final MaterialCard setTitle(Text title) {
        layout.getChildren().remove(this.title);
        this.title = title;
        title.setFont(Font.font("Roboto Black", 14));
        layout.add(title, 0, 0);
        return this;
    }

    public final MaterialCard setHeaderText(Text header) {
        layout.getChildren().remove(this.header);
        this.header = header;
        header.setFont(Font.font("Roboto"));
        layout.add(header, 0, 1);
        return this;
    }

    public final MaterialCard setFooterText(Text footer) {
        layout.getChildren().remove(this.footer);
        this.footer = footer;
        footer.setFont(Font.font("Roboto"));
        layout.add(footer, 0, 2);
        return this;
    }

    public final MaterialCard setActions(JFXButton modifica, JFXButton elimina) {
        modifica.setStyle("-fx-background-color: transparent");
        GridPane.setColumnSpan(modifica, GridPane.REMAINING);
        GridPane.setValignment(modifica, VPos.BOTTOM);
        GridPane.setVgrow(modifica, Priority.ALWAYS);
        modifica.setBackground(this.getBackground());
        if (isBackgroundDark()) modifica.setTextFill(Color.WHITE);
        layout.add(modifica, 0, 3);

        elimina.setStyle("-fx-background-color: transparent");
        GridPane.setColumnSpan(elimina, GridPane.REMAINING);
        GridPane.setValignment(elimina, VPos.BOTTOM);
        GridPane.setHgrow(elimina, Priority.ALWAYS);
        if (isBackgroundDark()) elimina.setTextFill(Color.RED);
        else elimina.setTextFill(Color.DARKRED.brighter());

        layout.add(elimina, 1, 3);

        rippler.setControl(layout);
        return this;
    }

    public final MaterialCard randomBackground() {
        MaterialCard buf = this;

        buf.setBackground(new Background(new BackgroundFill(Color.color(
                ThreadLocalRandom.current().nextDouble(),
                ThreadLocalRandom.current().nextDouble(),
                ThreadLocalRandom.current().nextDouble()
        ).brighter().brighter().saturate().brighter(), CornerRadii.EMPTY, Insets.EMPTY)));

        if (isBackgroundDark()) {
            title.setFill(Color.WHITE);
            setRipplerFill(Color.WHITE);
            DropShadow ds = new DropShadow(BlurType.GAUSSIAN, Color.color(0.2,0.2,0.2), 2.0, 0.0, 0.0, 2.0);
            title.setEffect(ds);
        }

        return buf;
    }

    public final MaterialCard setBackground(Color newBackground) {
        MaterialCard buf = this;
        buf.setBackground(new Background(new BackgroundFill(newBackground, CornerRadii.EMPTY, Insets.EMPTY)));

        if (isBackgroundDark()) {
            title.setFill(Color.WHITE);
            setRipplerFill(Color.WHITE);
            DropShadow ds = new DropShadow(BlurType.GAUSSIAN, Color.color(0.2,0.2,0.2), 2.0, 0.0, 0.0, 2.0);
            title.setEffect(ds);

            header.setFill(Color.LIGHTGRAY);
            footer.setFill(Color.LIGHTGRAY);
        } else {
            title.setFill(Color.BLACK);
            header.setFill(Color.BLACK);
            footer.setFill(Color.BLACK);
            title.setEffect(null);
        }

        return buf;
    }

    public final Color getBackgroundColor() {
        return (Color)this.getBackground().getFills().get(0).getFill();
    }

    private void initializeListeners() {
        this.setOnMouseClicked(event -> {
            if (event.getY() > this.getHeight()/1.3 && event.getX() < this.getWidth()/2) {
                Optional<ModificaProperties> result =
                        new Dialogs()
                                .new Modifica(this.bound)
                                .withColor(getBackgroundColor())
                                .show();
                if (result.isPresent()) {
                    if (!result.get().getPersone().equals("")) {
                        //Persone
                        this.bound.setPersone(Integer.parseInt(result.get().getPersone()));
                        this.setHeaderText(new Text("Persone: " + bound.getPersone()));
                    }
                    if (!result.get().getPosti().equals("")) {
                        //Posti
                        this.bound.setPosti(Integer.parseInt(result.get().getPosti()));
                        this.setFooterText(new Text("Posti: " + bound.getPosti()));
                    }

                    if (this.bound.isFull()) this.setTitle(new Text(this.title.getText() + " - PIENO"));

                    this.bound.setOrdini(result.get().getOrdini());

                    //Colore
                    this.setBackground(result.get().getColor());
                    this.bound.setColor(result.get().getColor());

                    JsonManager.change(this.bound.getId(), this.bound);
                }
            }
            if (event.getY() > this.getHeight()/1.3 && event.getX() > this.getWidth()/2) {
                Optional<ButtonType> result = new Dialogs().new Elimina(this.bound).show();
                if (result.isPresent())
                    if (result.get() == ButtonType.OK) {
                        JsonManager.removeAll(this.bound);
                        fireEvent(new RecordEvent(this, null, RecordEvent.RECORD_DELETED));
                    }
            }
        });
    }

    //////////////
    //  Utility //
    //////////////
    private boolean isBackgroundDark() {
        return ((Color)this.getBackground().getFills().get(0).getFill()).grayscale().getBrightness() < 0.6;
    }

    public final boolean isSquare() {
        return this.height == this.width;
    }

    public static MaterialCard getDummy(double width) {
        return new MaterialCard(width, "Dummy", new Tavolo())
                .setHeaderText(new Text("Header"))
                .setFooterText(new Text("Footer"))
                .randomBackground();
    }
}


