package ui;

import com.jfoenix.controls.*;
import com.jfoenix.controls.cells.editors.DoubleTextFieldEditorBuilder;
import com.jfoenix.controls.cells.editors.TextFieldEditorBuilder;
import com.jfoenix.controls.cells.editors.base.GenericEditableTreeTableCell;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import data.ModificaProperties;
import data.Ordine;
import data.Tavolo;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.TreeTableColumn.CellEditEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

/**
 * Created by Leonardo Baldin on 20/08/17.
 *
 * @author Leonardo Baldin
 */
public class Dialogs {

    @SuppressWarnings("unchecked")
    public class Modifica extends AbstractDialog<Modifica> {

        private String title = "Modifica tavolo: ";
        private String header;
        private String contextText;
        private Tavolo currentSelected;
        private ObservableList<Ordine> diffOrdini = FXCollections.observableArrayList();

        Dialog<ModificaProperties> dialog;
        private JFXColorPicker colorPicker = new JFXColorPicker();
        private JFXTreeTableView<Ordine> ordine = new JFXTreeTableView<>();
        private JFXTreeTableColumn<Ordine, String> itemColumn = new JFXTreeTableColumn<>("Oggetto");
        private JFXTreeTableColumn<Ordine, Double> prezzoColumn = new JFXTreeTableColumn<>("Prezzo");

        public Modifica(Tavolo tavolo) {
            this.currentSelected = tavolo;
            title += tavolo.getId();
            dialog = new Dialog<>();
        }

        public Modifica withColor(Color color) {
            colorPicker.setValue(color);
            return this;
        }

        @Override
        public Modifica setTitle(String title) {
            this.title = title;
            return this;
        }

        @Override
        public Modifica setHeader(String header) {
            this.header = header;
            return this;
        }

        @Override
        public Modifica setContextText(String text) {
            this.contextText = text;
            return this;
        }

        @Override
        public Optional<ModificaProperties> show() {
            dialog.setTitle(this.title);
            dialog.setHeaderText(this.header);
            dialog.setContentText(this.contextText);
            dialog.getDialogPane().setContent(makeLayout());
            return dialog.showAndWait();
        }

        @Override
        protected GridPane makeLayout() {
            dialog.getDialogPane().setMinWidth(700);
            dialog.getDialogPane().setMinHeight(200);
            dialog.getDialogPane().autosize();

            GridPane grid = new GridPane();
            ColumnConstraints columnConstraints = new ColumnConstraints();
            columnConstraints.setFillWidth(true);
            columnConstraints.maxWidthProperty().bind(dialog.getDialogPane().widthProperty());
            columnConstraints.setHgrow(Priority.ALWAYS);
            grid.getColumnConstraints().add(columnConstraints);
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setAlignment(Pos.CENTER);
            grid.setPrefSize(dialog.getWidth(), dialog.getHeight());
            grid.setPadding(new Insets(20, 10, 10, 10));

            TextField persone = new TextField();
            persone.setPromptText(String.valueOf(currentSelected.getPersone()));
            TextField posti = new TextField();
            posti.setPromptText(String.valueOf(currentSelected.getPosti()));
            grid.add(new Label("Persone:"), 0, 0);
            grid.add(persone, 1, 0);
            grid.add(new Label("Posti:"), 0, 1);
            grid.add(posti, 1, 1);
            grid.add(new Label("Colore:"), 0, 2);
            grid.add(colorPicker, 1, 2);

            setupCellValueFactory(itemColumn, Ordine::getItemProperty);
            setupCellValueFactory(prezzoColumn, o -> o.getPrezzoProperty().asObject());
            itemColumn.setMinWidth(150);
            itemColumn.setResizable(true);
            itemColumn.setCellFactory(
                    (TreeTableColumn<Ordine, String> column) -> new GenericEditableTreeTableCell<>(
                            new TextFieldEditorBuilder())
            );
            itemColumn.setOnEditCommit((CellEditEvent<Ordine, String> event) ->
                event.getTreeTableView()
                    .getTreeItem(event.getTreeTablePosition().getRow())
                    .getValue().getItemProperty().set(event.getNewValue())
            );
            prezzoColumn.setMinWidth(50);
            prezzoColumn.setResizable(true);
            prezzoColumn.setCellFactory((TreeTableColumn<Ordine, Double> param) -> new GenericEditableTreeTableCell<>(
                    new DoubleTextFieldEditorBuilder())
            );
            prezzoColumn.setOnEditCommit((CellEditEvent<Ordine, Double> event) ->
                    event.getTreeTableView()
                            .getTreeItem(event.getTreeTablePosition().getRow())
                            .getValue().getPrezzoProperty().set(event.getNewValue())
            );

            // TreeViewHeader
            HBox treeViewHeader = new HBox();
            treeViewHeader.setAlignment(Pos.CENTER);
            treeViewHeader.setSpacing(10);
            treeViewHeader.getChildren().add(new Label("Ordine"));

            // Item Count
            Label itemCount = new Label();
            itemCount.textProperty().bind(
                    Bindings.createStringBinding(
                            () -> "(" + (ordine.getExpandedItemCount() - 1) + ")",
                            ordine.expandedItemCountProperty()
                    )
            );
            treeViewHeader.getChildren().add(itemCount);

            // Add Remove
            JFXButton add = new JFXButton("+");
            add.setStyle(
                    "-fx-padding: 0.5em 0.57em;" +
                    "-fx-font-size: 16px;" +
                    "-jfx-button-type: RAISED;" +
                    "-fx-text-fill: WHITE;" +
                    "-fx-background-color: #00c853"
            );
            JFXButton remove = new JFXButton("-");
            remove.setStyle(
                    "-fx-padding: 0.5em 0.8em;" +
                    "-fx-font-size: 16px;" +
                    "-jfx-button-type: RAISED;" +
                    "-fx-text-fill: WHITE;" +
                    "-fx-background-color: #ff3027"
            );
            add.setOnMouseClicked(click -> {
                diffOrdini.add(new Ordine("", 0.0));
                this.currentSelected.addOrdini(diffOrdini);
                ordine.setRoot(new RecursiveTreeItem<>(this.currentSelected.getOrdini(), RecursiveTreeObject::getChildren));
            });
            treeViewHeader.getChildren().add(add);
            remove.setOnMouseClicked(click -> {
                Ordine selection = ordine.getSelectionModel().getSelectedItem().getValue();
                diffOrdini.remove(selection);
                this.currentSelected.removeOrdini(selection);
                ordine.setRoot(new RecursiveTreeItem<>(this.currentSelected.getOrdini(), RecursiveTreeObject::getChildren));
            });
            treeViewHeader.getChildren().add(remove);

            // Search
            Region blank = new Region();
            treeViewHeader.getChildren().add(blank);
            HBox.setHgrow(blank, Priority.ALWAYS);
            StackPane searchContainer = new StackPane();
            //HBox.setHgrow(searchContainer, Priority.ALWAYS);
            searchContainer.setAlignment(Pos.CENTER_RIGHT);
            JFXTextField searchField = new JFXTextField();
            searchField.setPromptText("Cerca...");
            searchContainer.setMaxWidth(150);
            searchField.textProperty().addListener(setupSearch(ordine));
            searchContainer.getChildren().add(searchField);
            treeViewHeader.getChildren().add(searchContainer);

            GridPane.setColumnSpan(treeViewHeader, GridPane.REMAINING);
            GridPane.setFillWidth(treeViewHeader, true);
            grid.add(treeViewHeader, 0, 3);

            ordine.setRoot(new RecursiveTreeItem<>(this.currentSelected.getOrdini(), RecursiveTreeObject::getChildren));
            ordine.getColumns().setAll(itemColumn, prezzoColumn);
            ordine.setShowRoot(false);
            ordine.setEditable(true);
            GridPane.setFillWidth(ordine,true);
            GridPane.setColumnSpan(ordine, GridPane.REMAINING);
            grid.add(ordine, 0, 4);

            // Conferma & return
            ButtonType conferma = new ButtonType("Conferma", ButtonBar.ButtonData.OK_DONE);
            dialog.getDialogPane().getButtonTypes().setAll(conferma, ButtonType.CANCEL);

            dialog.setResultConverter(pressed -> {
                if (pressed.equals(conferma)) return new ModificaProperties(
                        persone.getText(),
                        posti.getText(),
                        colorPicker.getValue(),
                        deserializeTree(ordine)
                );
                return null;
            });

            return grid;
        }

        private <T> void setupCellValueFactory(JFXTreeTableColumn<Ordine, T> column, Function<Ordine, ObservableValue<T>> mapper) {
            column.setCellValueFactory((TreeTableColumn.CellDataFeatures<Ordine, T> param) -> {
                if (column.validateValue(param)) return mapper.apply(param.getValue().getValue());
                else return column.getComputedValue(param);
            });
        }

        private ChangeListener<String> setupSearch(final JFXTreeTableView<Ordine> tableView) {
            return (o, oldVal, newVal) ->
                tableView.setPredicate(ordineTreeItem -> {
                    final Ordine ordine = ordineTreeItem.getValue();
                    return ordine.getItemProperty().get().contains(newVal)
                        || Double.toString(ordine.getPrezzoProperty().get()).contains(newVal);
                });
        }

        private List<Ordine> deserializeTree(JFXTreeTableView<Ordine> tableView) {
            ObservableList<Ordine> deserialized = FXCollections.observableArrayList();
            tableView.getRoot().getChildren()
                    .stream()
                    .filter(val -> !val.getValue().getItemProperty().getValue().isEmpty())
                    .forEach(cell -> deserialized.add(cell.getValue()));

            return deserialized;
        }

    }

    @SuppressWarnings("unchecked")
    public class Conto extends AbstractDialog<Conto> {

        private String title = "Conto";
        private String header;
        private String text;

        @Override
        public Conto setTitle(String title) {
            this.title = title;
            return this;
        }

        @Override
        public Conto setHeader(String header) {
            this.header = header;
            return this;
        }

        @Override
        public Conto setContextText(String text) {
            return null;
        }

        @Override
        public Conto show() {
            return this;
        }

        @Override
        protected Pane makeLayout() {
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    public class Info extends AbstractDialog<Info> {

        private String title = "Info";
        private String header;
        private Tavolo currentSelected;

        public Info(Tavolo tavolo) {
            this.currentSelected = tavolo;
        }

        @Override
        public Info setTitle(String title) {
            this.title = title;
            return this;
        }

        @Override
        public Info setHeader(String header) {
            this.header = header;
            return this;
        }

        @Override
        public Info setContextText(String text) {
            return null;
        }

        @Override
        public Info show() {
            return this;
        }

        @Override
        protected Pane makeLayout() {
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    public class Elimina extends AbstractDialog<Elimina> {

        private String title = "Elimina";
        private String header = "Elimina tavolo ";
        private String contextText = "Vuoi eliminare il tavolo?";
        private Tavolo currentSelected;

        private javafx.scene.control.Alert dialog;

        public Elimina(Tavolo tavolo) {
            this.currentSelected = tavolo;
            this.header += currentSelected.getId() + 1;
        }

        @Override
        public Elimina setTitle(String title) {
            this.title = title;
            return this;
        }

        @Override
        public Elimina setHeader(String header) {
            this.header = header;
            return this;
        }

        @Override
        public Elimina setContextText(String text) {
            this.contextText = text;
            return this;
        }

        @Override
        public Optional<ButtonType> show() {
            dialog = new javafx.scene.control.Alert(javafx.scene.control.Alert.AlertType.CONFIRMATION);
            dialog.setTitle(this.title);
            dialog.setHeaderText(this.header);
            dialog.setContentText(this.contextText);

            return dialog.showAndWait();
        }

        @Override
        protected Pane makeLayout() {
            return null;
        }
    }

    private abstract class AbstractDialog<T> {

        protected String title;
        protected String header;
        protected Tavolo currentSelected;

        public abstract T setTitle(String title);
        public abstract T setHeader(String header);
        public abstract T setContextText(String text);
        public abstract <R> R show();
        protected abstract Pane makeLayout();

    }

}
